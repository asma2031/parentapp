package parent.webapp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CalendarTest extends TestBase {
	LoginPage loginpageObject;
	HomePage homePageObject;
	DashboardPage dashboardPageObject;
	CalendarPage calendarPageObject;
	
	@Test(priority=1,alwaysRun=true)
	public void Login() throws InterruptedException {

		loginpageObject = new LoginPage(driver);
		loginpageObject.UserLoginForum("demo@parent.eu", "12345678");
		//String actualURL ="https://portal-staging.parent.cloud/institute";
		Thread.sleep(3000);
		Assert.assertEquals("Institute", loginpageObject.hometitle.getText());
		
		
	}
	@Test(priority=2)
	public void OpenCalendarPage() throws InterruptedException
	{
		homePageObject = new HomePage(driver);
		dashboardPageObject= new DashboardPage(driver);
		homePageObject.KidsPalaceFun();
		Thread.sleep(3000);
		dashboardPageObject.OpenCalendarPage();
	}
	
	@Test(priority=3)
	public void AddNewEvent() {
		calendarPageObject = new CalendarPage(driver);
		calendarPageObject.AddEvent();
		
	}

}
