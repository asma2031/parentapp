package parent.webapp;

import org.testng.Assert;
import org.testng.annotations.Test;


public class HomeTest extends TestBase{
	LoginPage loginpageObject;
	HomePage homePageObject;
	
		@Test
		public void OpenKidsPalaceTest() throws InterruptedException {

			loginpageObject = new LoginPage(driver);
			homePageObject = new HomePage(driver);
			loginpageObject.UserLoginForum("demo@parent.eu", "12345678");
			//String actualURL ="https://portal-staging.parent.cloud/institute";
			Thread.sleep(3000);
			Assert.assertEquals("Institute", loginpageObject.hometitle.getText());
			homePageObject.KidsPalaceFun();
			
		}

}
