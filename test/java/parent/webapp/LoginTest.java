package parent.webapp;
import org.testng.Assert;
import org.testng.annotations.Test;


public class LoginTest extends TestBase {
	LoginPage loginPageObject;
	
	@Test
	public void login() throws InterruptedException {

		loginPageObject = new LoginPage(driver);
		loginPageObject.UserLoginForum("demo@parent.eu", "12345678");
		//String actualURL ="https://portal-staging.parent.cloud/institute";
		Thread.sleep(3000);
		Assert.assertEquals("Institute", loginPageObject.hometitle.getText());
		
	}

}
