package parent.webapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends PageBase {

	public DashboardPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@FindBy(id="calendarTab")
	WebElement calendarBtn;
	
	public void OpenCalendarPage()
	{
		clickButton(calendarBtn);
	}

}
