package parent.webapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase{

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(xpath="//input[@type='text'][@id='txtEmail']")
	WebElement emailtxt;

	@FindBy(xpath="//input[@type='password'][@id='txtPassword']")
	WebElement passwordtxt;

	@FindBy(xpath="//button[contains(text(),'Log in')]")
	WebElement loginbtn;
	
	@FindBy(xpath="//div[text()='Institute']")
	public WebElement hometitle;

	public void UserLoginForum(String email , String password) 
	{
		setTextElementText(emailtxt, email);
		setTextElementText(passwordtxt, password);
		clickButton(loginbtn);
	}


}
