package parent.webapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CalendarPage extends PageBase{

	public CalendarPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(id="createEventBtn")
	WebElement createEventBtn;
	
	@FindBy(id="createWeeklyScheduleBtn")
	WebElement createWeeklyScheduleBtn;
	
	
	public void AddEvent() {
		clickButton(createEventBtn);
	}
	
	public void AddWeeklySchedule() {
		clickButton(createWeeklyScheduleBtn);
	}
}
