package parent.webapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase{

	public HomePage(WebDriver driver) {
		super(driver);
	}
	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/app-main-layout[1]/main[1]/app-institution[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[5]")
	WebElement kidpalacesec;
	
	public void KidsPalaceFun()
	{
			clickButton(kidpalacesec);
	}


}
